package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class JsExecutorDdemo {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.automationtesting.in/Register.html");
		driver.manage().window().maximize();

		// using action class can perform scroll down
//	Actions a=new Actions(driver);
//	Thread.sleep(3000);
//	a.sendKeys(Keys.ARROW_DOWN).perform();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.getElementById('yearbox').ScrollIntoView()");
	}
}
