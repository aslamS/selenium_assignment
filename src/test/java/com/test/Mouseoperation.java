package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Mouseoperation {
public static void main(String[] args) {
	WebDriver driver=new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.findElement(By.xpath("(//a[contains(text(),'Books')])[1]")).click();
	WebElement sort = driver.findElement(By.id("products-orderby"));
	Select s=new Select(sort);
	s.selectByIndex(3);
}
}
